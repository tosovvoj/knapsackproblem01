/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dynamic;

import cz.cvut.paaknapsackproblem01.Knapsack;
import cz.cvut.paaknapsackproblem01.ProcessInfo;
import cz.cvut.paaknapsackproblem01.Solver;
import static cz.cvut.paaknapsackproblem01.Solver.weights;
import java.util.Arrays;

/**
 *
 * @author Vojtech Tosovsky
 */
public class FPTAS extends Solver {

    public static int roundingCoefficient = 32;
//    private int[] previous;
//    private int[] akt;
    private int[][] array;
    private int maxPrice;
    private int priceBackup[];

    @Override
    public Knapsack solve(ProcessInfo processInfo) {
        init();
        fillInTables();
        for (int price = maxPrice; price >= 0; price--) {
            if (array[stuffCnt][price] <= Knapsack.capacity) {
                boolean[] usedStuff = getUsedStufff(price);
                Knapsack solution = new Knapsack(usedStuff);
                int totalPrice = 0;
                for (int i = 0; i < usedStuff.length; i++) {
                    if (usedStuff[i] != false) {
                        totalPrice += priceBackup[i];
                    }
                }
                solution.toatlPrice = totalPrice;
                return solution;
            }
        }
        return null;
    }

    private void fillInTables() {
        for (int stuffIndex = 1; stuffIndex <= stuffCnt; stuffIndex++) {
            for (int price = 0; price <= maxPrice; price++) {
                //           index >= 0                            previous price != inf
                if ((price - prices[stuffIndex - 1]) >= 0 && ((array[stuffIndex - 1][price - prices[stuffIndex - 1]]) != Integer.MAX_VALUE)) {
                    if (array[stuffIndex - 1][price] < (array[stuffIndex - 1][price - prices[stuffIndex - 1]] + weights[stuffIndex - 1])) {
                        array[stuffIndex][price] = array[stuffIndex - 1][price];
                    } else {
                        array[stuffIndex][price] = (array[stuffIndex - 1][price - prices[stuffIndex - 1]] + weights[stuffIndex - 1]);
                    }
                } else {
                    array[stuffIndex][price] = array[stuffIndex - 1][price];
                }
            }
        }
    }

    private void init() {
        priceBackup = Arrays.copyOf(prices, prices.length);
        if (roundingCoefficient != 0) {
            for (int i = 0; i < prices.length; i++) {
                prices[i] /= roundingCoefficient;

            }
        }
        maxPrice = 0;
        for (int i = 0; i < prices.length; i++) {
            maxPrice += prices[i];
        }
        array = new int[stuffCnt + 1][maxPrice + 1];
        for (int i = 0; i < maxPrice + 1; i++) {
            array[0][i] = Integer.MAX_VALUE;
        }
        for (int i = 0; i <= stuffCnt; i++) {
            array[i][0] = 0;
        }

    }

    private boolean[] getUsedStufff(int fianlPrice) {
        boolean[] usedStuff = new boolean[stuffCnt];
        int price = fianlPrice;
        for (int stuffIndex = stuffCnt; stuffIndex >= 1; stuffIndex--) {
            if (!(array[stuffIndex][price] == array[stuffIndex - 1][price])) {
                usedStuff[stuffIndex - 1] = true;
                price -= prices[stuffIndex - 1];
            }

        }
        return usedStuff;
    }
}
