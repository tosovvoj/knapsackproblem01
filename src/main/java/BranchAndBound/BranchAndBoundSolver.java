/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BranchAndBound;

import cz.cvut.paaknapsackproblem01.Knapsack;
import cz.cvut.paaknapsackproblem01.ProcessInfo;
import cz.cvut.paaknapsackproblem01.Solver;
import java.util.Arrays;

/**
 *
 * @author Vojtech Tosovsky
 */
public class BranchAndBoundSolver extends Solver {

    private static Knapsack bestKnapsack;
    private static Knapsack emptyKnapsack;

    @Override
    public Knapsack solve(ProcessInfo processInfo) {
        boolean[] usedSutff = new boolean[stuffCnt];
        boolean[] copyOf = Arrays.copyOf(usedSutff, usedSutff.length);
        boolean[] copyOf2 = Arrays.copyOf(usedSutff, usedSutff.length);
        bestKnapsack = new Knapsack(copyOf);
        emptyKnapsack = new Knapsack(copyOf2);
        return recursionSolve(stuffCnt, 0, new Knapsack(usedSutff));
    }

    private Knapsack recursionSolve(int aviableStuffcnt, int actualStuffIndex, Knapsack knapsack) {
        if (aviableStuffcnt == 0) {
            registerIfbest(knapsack);
            return knapsack;
        }
        if (!sense(aviableStuffcnt, actualStuffIndex, knapsack)) {
            return emptyKnapsack;
        }
        //without i stuff
        aviableStuffcnt--;
        actualStuffIndex++;
        Knapsack withoutIstuff = recursionSolve(aviableStuffcnt, actualStuffIndex, knapsack);
        actualStuffIndex--;

        //with i stuff
        Knapsack withIstuff = new Knapsack(knapsack, true);
        withIstuff.addIStuff(actualStuffIndex);
        actualStuffIndex++;
        withIstuff = recursionSolve(aviableStuffcnt, actualStuffIndex, withIstuff);
        if (withIstuff.isOverload()) {
            registerIfbest(withoutIstuff);
            return withoutIstuff;
        } else {
            if (withIstuff.toatlPrice > withoutIstuff.toatlPrice) {
                registerIfbest(withIstuff);
                return withIstuff;
            } else {
                registerIfbest(withoutIstuff);
                return withoutIstuff;
            }
        }
    }

    private void registerIfbest(Knapsack knapsack) {
        if ((knapsack.toatlPrice > bestKnapsack.toatlPrice) && !knapsack.isOverload()) {
            bestKnapsack = knapsack;
        }
    }

    private static int firstTrue(boolean[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i]) {
                return i;
            }
        }
        return -1;
    }

    private boolean sense(int aviableStuffcnt, int actualStuffIndex, Knapsack knapsack) {
        int maxPossiblePrice = knapsack.toatlPrice;
        if (knapsack.isOverload()) {
            return false;
        }

        for (int i = actualStuffIndex; i < stuffCnt; i++) {
            maxPossiblePrice += prices[i];
        }
        return maxPossiblePrice >= bestKnapsack.toatlPrice;
    }

}
