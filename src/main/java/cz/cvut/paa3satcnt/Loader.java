/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.paa3satcnt;

import com.google.common.primitives.Ints;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Vojtech Tosovsky
 */
public class Loader {

    private static final String PROBLEM_LINE_PREFIX = "p cnf";

    public static CNF loadCnf(String fileName) throws FileNotFoundException, IOException {
        int variablesCnt = -1;
        int clauseCnt = -1;
        BufferedReader br = new BufferedReader(new FileReader(new File(fileName)));
        String line = "c";
        //skip all comments line
        while (line != null && line.startsWith("c")) {
            line = br.readLine();
        }
        // first line is the problem description line p FORMAT VARIABLES CLAUSES
        if (line.startsWith(PROBLEM_LINE_PREFIX)) {
            Scanner scanner = new Scanner(line.substring(PROBLEM_LINE_PREFIX.length()));
            variablesCnt = scanner.nextInt();
            clauseCnt = scanner.nextInt();
        }
        CNF cnt = new CNF(clauseCnt, variablesCnt);
        line = br.readLine();
        StringBuilder sb = new StringBuilder();
        while (line != null) {
            if (!line.startsWith("c")) {
                sb.append(line);
                sb.append(' ');
            }
            line = br.readLine();
        }
        Scanner scanner = new Scanner(sb.toString());
        for (int clauseNumber = 0; clauseNumber < clauseCnt; clauseNumber++) {
            int varNumber = scanner.nextInt();
            List<Integer> literals = new ArrayList<Integer>();
            while (varNumber != 0) {
                literals.add(varNumber);
                varNumber = scanner.nextInt();
            }
            int[] ints = Ints.toArray(literals);
            Clause clause = new Clause(ints);
            cnt.setClause(clauseNumber, clause);
        }
        return cnt;
    }
}
