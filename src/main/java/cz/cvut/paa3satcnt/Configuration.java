/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.paa3satcnt;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author Vojtech Tosovsky
 */
public class Configuration {

    private static final Random random = new Random();
    private CNF cnf;
    private boolean[] variables;
    private int weight;
    private int fulfilledClausuleCnt;
    private int price;
    int unfulfilledClCnt = -1;
    private static int FFBONUS = 10;
    private static int UFFPENALTY = 520;

    static Configuration randomInitialize(CNF cnf, int inFlag) {
        Configuration configuration = new Configuration();
        configuration.cnf = cnf;
        boolean[] variables = new boolean[cnf.varaiblesCnt + 1];
        switch (inFlag) {
            case 1:
                randmoInitVariables(variables);
                break;
            case 2:
                trueInitVariables(variables);
                break;
            default: // all variables are false;
                break;

        }
        randmoInitVariables(variables);
        configuration.variables = (variables);
        configuration.evaluate();
        return configuration;
    }

    private static void randmoInitVariables(boolean[] variables) {
        for (int i = 0; i < variables.length; i++) {
            if (random.nextBoolean()) {
                variables[i] = true;
            }
        }
    }

    private static void trueInitVariables(boolean[] variables) {
        for (int i = 0; i < variables.length; i++) {
            variables[i] = true;
        }
    }

    private Configuration() {

    }

    public Configuration(Configuration c) {
        cnf = new CNF(c.getCnt());
        variables = Arrays.copyOf(c.getVariables(), c.getVariables().length);
        evaluate();
    }

    private CNF getCnt() {
        return cnf;
    }

    public void evaluate() {
        evaluateFulfilledClausuleCnt();
        evaluateWeight();
        evaluatePrice();
    }

    private int evaluateFulfilledClausuleCnt() {
        fulfilledClausuleCnt = 0;
        Clause[] clausules = cnf.getClauses();
        for (int i = 0; i < clausules.length; i++) {
            int[] lit = clausules[i].literals;
            if (literalValue(lit)) {
                fulfilledClausuleCnt++;
            }
        }
        return fulfilledClausuleCnt;

    }

    private void setFulfilledClausuleCnt(int fulfilledClausuleCnt) {
        this.fulfilledClausuleCnt = fulfilledClausuleCnt;
    }

    public boolean[] getVariables() {
        return variables;
    }

    public void switchVariables(int variable[], int validCnt) {
        for (int i = 0; i < validCnt; i++) {
            int variableIndex = variable[i];
            switchVariableAndChangeWeight(variableIndex);
        }
        evaluateFulfilledClausuleCnt();
        evaluatePrice();
    }

    public void undoSwitchVariables(int validCNt, int variable[], int ffc, int weight, int price) {
        for (int i = 0; i < validCNt; i++) {
            int variableIndex = variable[i];
            switchVariableAndChangeWeight(variableIndex);
        }
        this.fulfilledClausuleCnt = ffc;
        this.weight = weight;
        this.price = price;
    }

    private void switchVariableAndChangeWeight(int index) {
        variables[index] = (!variables[index]);
        if (variables[index]) {
            weight += cnf.getWeights()[index];
        } else {
            weight -= cnf.getWeights()[index];
        }

    }

    public int evaluateWeight() {
        weight = 0;
        for (int i = 1; i < variables.length; i++) {
            if (variables[i]) {
                weight += cnf.getWeights()[i];
            }
        }
        return weight;
    }

    public int evaluatePrice() {
        price = 0;
        price += weight;
        unfulfilledClCnt = (cnf.getClauses().length - fulfilledClausuleCnt);
        price -= ((cnf.getClauses().length - fulfilledClausuleCnt) * UFFPENALTY);
        if (unfulfilledClCnt == 0) {
            price += FFBONUS;
        }
        return price;
    }

    private boolean literalValue(int[] lit) {
        for (int i = 0; i < lit.length; i++) {
            if (lit[i] < 0) {
                //variable is in negation
                if (!(variables[-lit[i]])) {
                    return true;
                }
            } else {
                //variable is not in negation
                if (variables[lit[i]]) {
                    return true;
                }
            }
        }
        return false;
    }

    public int getWeight() {
        return weight;
    }

    public int getFulfilledClausuleCnt() {
        return fulfilledClausuleCnt;
    }

    public int getPrice() {
        return price;
    }

    public int getUnfulfilledClCnt() {
        return unfulfilledClCnt;
    }

    @Override
    public String toString() {
        return "Con{" + "weight=" + weight + ", ffc=" + fulfilledClausuleCnt + ", price=" + price + '}';
    }

    public static void setFFBONUS(int FFBONUS) {
        Configuration.FFBONUS = FFBONUS;
    }

    public static void setUFFPENALTY(int UFFPENALTY) {
        Configuration.UFFPENALTY = UFFPENALTY;
    }

}
