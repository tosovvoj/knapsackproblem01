/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.paa3satcnt;

import java.util.Random;

/**
 *
 * @author Vojtech Tosovsky
 */
public class SimulateAnnealingSolver {

    private final Random random = new Random();
    private int metropolisMax = 60;//80
    private double startingTemerature = 300;
    private double temperature;
    private double minTemperature = 10;
    private double collingRate = 0.997;
    private int[] actSwVariables;
    private Configuration bestConfiguration;
    private Configuration actConfiguration;
    private int variablesCnt = 0;
    int actSwitchedVariablesCNT;
    int maxSwithcedVariables = 2;
    int lastPrice = -1;
    int lastWeight = -1;
    int lastFFc = -1;
    ProcessInfo processInfo;
    int stepCounter = 0;
    public static final int LOGSTEPMODULO = 256;

    public SimulateAnnealingSolver(int startTemperature, double collingRate, double minTemperature, int maxSwithcedVariables, int metropolisMAX) {
        if (startTemperature != -1) {
            this.startingTemerature = startTemperature;
        }
        if (minTemperature != -1) {
            this.minTemperature = minTemperature;
        }
        if (collingRate != -1) {
            this.collingRate = collingRate;
        }
        if (maxSwithcedVariables != -1) {
            this.maxSwithcedVariables = maxSwithcedVariables;
        }
        if (metropolisMAX != -1) {
            this.metropolisMax = metropolisMAX;
        }
    }

    public ProcessInfo solve(CNF cnt, int inFlag) {
        stepCounter = 0;
        processInfo = new ProcessInfo();
        Configuration configuration = Configuration.randomInitialize(cnt, inFlag);
        actConfiguration = configuration;
        bestConfiguration = new Configuration(actConfiguration);
        variablesCnt = actConfiguration.getVariables().length;
        actSwVariables = new int[maxSwithcedVariables];
        anneal();
        processInfo.configuration = bestConfiguration;
        processInfo.stepCnt = stepCounter;
        return processInfo;

    }

    private void anneal() {
        temperature = startingTemerature;
        int cnt = 0;
//        System.out.println(Arrays.toString(actSwitchedVariables));
        while (temperature > minTemperature) {
//            System.out.println("temperature: " + temperature + " Cnt: " + cnt + " act:" + actConfiguration + " best:" + bestConfiguration);
            cnt++;
            metropolis();
            temperature *= collingRate;
        }
//        System.out.println(Arrays.toString(actSwitchedVariables));
    }

    private void metropolis() {
        for (int i = 0; i < metropolisMax; i++) {
            double priceImprovment = randomChangeConfiguratio();
            if (priceImprovment > 0) {
                if ((bestConfiguration.getPrice() < actConfiguration.getPrice()) && actConfiguration.unfulfilledClCnt == 0) {
                    bestConfiguration = new Configuration(actConfiguration);
                }
            } else {
                double criterion = Math.exp(priceImprovment / temperature);
                if (!(random.nextDouble() < criterion)) {
                    actConfiguration.undoSwitchVariables(actSwitchedVariablesCNT, actSwVariables, lastFFc, lastWeight, lastPrice);
                }
            }
        }
    }

    private double randomChangeConfiguratio() {
        stepCounter++;
        int priceBefore = actConfiguration.getPrice();
        lastFFc = actConfiguration.getFulfilledClausuleCnt();
        lastPrice = priceBefore;
        lastWeight = actConfiguration.getWeight();

        actSwitchedVariablesCNT = 1 + random.nextInt(maxSwithcedVariables);

        for (int i = 0; i < actSwitchedVariablesCNT; i++) {
            actSwVariables[i] = 1 + random.nextInt(variablesCnt - 1);
        }

        actConfiguration.switchVariables(actSwVariables, actSwitchedVariablesCNT);
        if (stepCounter % LOGSTEPMODULO == 0) {
            processInfo.notch(actConfiguration.getPrice(), temperature, actConfiguration.getUnfulfilledClCnt(), bestConfiguration.getPrice());
        }
        int priceAfter = actConfiguration.getPrice();
        return (priceAfter - priceBefore);

    }

}
