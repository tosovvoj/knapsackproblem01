/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.paa3satcnt;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author Vojtech Tosovsky
 */
public class CNF {

    private Clause[] clauses;
    private int[] weights;
    public int varaiblesCnt;

    public CNF(int clauseCnt, int varaiblesCnt) {
        clauses = new Clause[clauseCnt];
        this.varaiblesCnt = varaiblesCnt;
    }

    void setClause(int i, Clause clause) {
        clauses[i] = clause;
    }

    public CNF(CNF cnt) {
        this.clauses = Arrays.copyOf(cnt.getClauses(), cnt.getClauses().length);
        this.weights = Arrays.copyOf(cnt.getWeights(), cnt.getWeights().length);
    }

    public CNF(Clause[] clauses, int[] weights) {
        this.clauses = clauses;
        this.weights = weights;
    }

    public void randomInitializeWeights(int seed, int maxWeight) {
        Random random = new Random(seed);
        weights = new int[varaiblesCnt + 1];
        for (int i = 1; i < weights.length; i++) {
            weights[i] = random.nextInt(maxWeight);
        }
        weights[0] = Integer.MIN_VALUE;
    }

    public Clause[] getClauses() {
        return clauses;
    }

    public int[] getWeights() {
        return weights;
    }

    @Override
    public String toString() {
        return "CNF{" + "clauses=" + clauses + ", weights=" + weights + ", varaiblesCnt=" + varaiblesCnt + '}';
    }

}
