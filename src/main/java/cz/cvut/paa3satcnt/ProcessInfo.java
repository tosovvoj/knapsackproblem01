/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.paa3satcnt;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Vojtech Tosovsky
 */
public class ProcessInfo {

    private final List<Double> temperatures;
    public Configuration configuration;
    private final List<Integer> prices;
    private final List<Integer> uffccs;
    private final List<Integer> bprices;
    private final List<Integer> buffccs;
    public int stepCnt = -1;

    public ProcessInfo() {
        temperatures = new ArrayList<Double>();
        prices = new ArrayList<Integer>();
        uffccs = new ArrayList<Integer>();
        bprices = new ArrayList<Integer>();
        buffccs = new ArrayList<Integer>();
    }

    public void notch(int price, double temperature, int uffcc, int bestPrice) {
        prices.add(price);
        temperatures.add(temperature);
        uffccs.add(uffcc);
        bprices.add(bestPrice);
    }

    public void print() {
        System.out.println("step , price , temperature, uffcs, bestprice");
        for (int i = 0; i < prices.size(); i++) {
//            System.out.println(i + ":" + " price:" + prices.get(i) + " temperatur:" + temperatures.get(i) + " uffcc:" + uffccs.get(i) + " bestPrice:" + bprices.get(i));
            System.out.println(i + "," + prices.get(i) + "," + temperatures.get(i) + "," + uffccs.get(i) + "," + bprices.get(i));
        }
    }

    public int getStepCnt() {
        return prices.size() * SimulateAnnealingSolver.LOGSTEPMODULO;
    }

    public List<Double> getTemperatures() {
        return temperatures;
    }

    public List<Integer> getPrices() {
        return prices;
    }

    public List<Integer> getUffccs() {
        return uffccs;
    }

    public List<Integer> getBprices() {
        return bprices;
    }

    public List<Integer> getBuffccs() {
        return buffccs;
    }

}
