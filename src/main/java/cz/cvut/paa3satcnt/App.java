package cz.cvut.paa3satcnt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) throws IOException {
        int clausuleCNT = 403;
        int backboneSize = 10;
        System.out.println("ClausuleCNT,weight, fails, step");
        double minTemperature = 5;
        int initTemperature = 500;
        double coolingRate = 0.999;
        int maxSwVariables = 3;
        int metropolisMAx = 100;
        int UFFKPENALTY = -1;
        int FFBONUS = -1;
        int[] clausuleCNTs = new int[]{403, 411, 418, 423, 435, 441, 449};
        for (int i = 1; i < clausuleCNTs.length; i++) {
            System.out.print(clausuleCNTs[i] + ",");
            solveOneSet(clausuleCNT, backboneSize, minTemperature, initTemperature, coolingRate, maxSwVariables, metropolisMAx, UFFKPENALTY, FFBONUS);

        }
    }

    private static void solveOneSet(int clausuleCNT, int backboneSize, double minTemperature, int initTemperature, double coolingRate, int maxSwithcedWariables, int metropolisMAx, int UFFKPENALTY, int FFBONUS) throws IOException {
        if (UFFKPENALTY != -1) {
            Configuration.setUFFPENALTY(UFFKPENALTY);
        }
        if (FFBONUS != -1) {
            Configuration.setFFBONUS(FFBONUS);
        }
        List<ProcessInfo> cumulateInfo = new ArrayList<ProcessInfo>();
        for (int i = 0; i < 20; i++) {
            CNF cnf = Loader.loadCnf("input/CBS_k3_n100_m" + clausuleCNT + "_b" + backboneSize + "/CBS_k3_n100_m" + clausuleCNT + "_b" + backboneSize + "_" + i + ".cnf");
//            CNF cnf = Loader.loadCnf("input/uf20-91/uf20-0" + i + ".cnf");
            cnf.randomInitializeWeights(i, 101);
            SimulateAnnealingSolver simulateAnnealingSolver = new SimulateAnnealingSolver(initTemperature, coolingRate, minTemperature, maxSwithcedWariables, metropolisMAx);
            int attempt = 1;
            long start = System.nanoTime();
            ProcessInfo processInfo = simulateAnnealingSolver.solve(cnf, attempt);
            Configuration configuration = processInfo.configuration;
            while (configuration.getFulfilledClausuleCnt() != clausuleCNT && attempt <= 9) {
//                System.out.println("opakuji Instance id" + i + "pokus: " + attempt);
                processInfo = simulateAnnealingSolver.solve(cnf, attempt % 3);
                configuration = processInfo.configuration;
//                System.out.println(configuration);
                attempt++;
            }
            long end = System.nanoTime();
            long timeInNano = end - start;
            long timeInMicro = timeInNano / 1000;
            long timeInMili = timeInMicro / 1000;
//            System.out.println("TimeinMicro: " + timeInMicro );
//            processInfo.print();
            cumulateInfo.add(processInfo);

//            System.out.println("");
//            System.out.println("Instance id" + i);
//            System.out.println(configuration);
        }
//        printAvgProcessInfo(provessInfos);
        printAvgresultInfo(cumulateInfo);
    }

    private static void printAvgProcessInfo(List<ProcessInfo> provessInfos) {
        int stepcnt = provessInfos.get(0).getStepCnt();
        int examplesCnt = provessInfos.size();
        System.out.println("price , bestprice, uffcc");
        for (int i = 0; i < stepcnt; i++) {
            int price = 0;
            int bestPrice = 0;
            int uffcc = 0;
            for (ProcessInfo processInfo : provessInfos) {
                price += processInfo.getPrices().get(i);
                bestPrice += processInfo.getBprices().get(i);
                uffcc += processInfo.getUffccs().get(i);
            }
            System.out.println(price / examplesCnt + "," + bestPrice / examplesCnt + "," + uffcc / examplesCnt);

        }
    }

    private static void printAvgresultInfo(List<ProcessInfo> processInfos) {
        double OKexamplesCnt = processInfos.size();
        double originExamplevnt = OKexamplesCnt;
        int weight = 0;
        double fail = 0;
        for (ProcessInfo processInfo : processInfos) {
            weight += processInfo.configuration.getWeight();
            if (processInfo.configuration.unfulfilledClCnt != 0) {
                fail++;
                OKexamplesCnt--;
                weight -= processInfo.configuration.getWeight();
            }
        }
//        System.out.println("weight , fails, steps");
        System.out.println(weight / OKexamplesCnt + "," + fail / originExamplevnt + "," + processInfos.get(0).stepCnt);

    }
}
