/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.paaknapsackproblem01;

import Annealing.SAProcessInfo;

/**
 *
 * @author Vojtech Tosovsky
 */
class Result {

    public Result(long time, double mistake, int instanceID) {
        this.time = time;
        this.mistake = mistake;
        this.instanceId = instanceID;
    }

    public Result(long time, double mistake, int instanceID, ProcessInfo processInfo) {
        this.time = time;
        this.mistake = mistake;
        this.instanceId = instanceID;
        this.processInfo = processInfo;
    }

    public int instanceId;
    public long time;
    public double mistake;
    public int maxPrice;
    public ProcessInfo processInfo;

}
