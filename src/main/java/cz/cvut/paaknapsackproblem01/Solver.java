/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.paaknapsackproblem01;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Vojtech Tosovsky
 */
public abstract class Solver {

    public int instanceID, stuffCnt;
    int knapsackCapacity;
    public static int[] weights;
    protected static int[] prices;
    private Scanner scanner;
    private boolean bruteForce = false;
    public int maxPrice;

    public void initialise(String fileName) throws FileNotFoundException {
        scanner = new Scanner(new File(fileName));

    }

    /**
     *
     * @return True if task was loaded, false if there is not any more task in
     * the file.
     */
    boolean loadTask() {
        maxPrice = 0;
        if (!scanner.hasNextInt()) {
            return false;
        }
        instanceID = scanner.nextInt();
        stuffCnt = scanner.nextInt();
        knapsackCapacity = scanner.nextInt();
        weights = new int[stuffCnt];
        prices = new int[stuffCnt];
        for (int i = 0; i < stuffCnt; i++) {
            weights[i] = (scanner.nextInt());
            prices[i] = (scanner.nextInt());
            if (maxPrice < prices[i]) {
                maxPrice = prices[i];
            }
        }
        Knapsack.capacity = (knapsackCapacity);
        return true;
    }

    public abstract Knapsack solve(ProcessInfo processInfo);

//    public abstract Knapsack solve();

}
