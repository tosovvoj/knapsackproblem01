/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.paaknapsackproblem01.BruteForce;

import cz.cvut.paaknapsackproblem01.Knapsack;
import cz.cvut.paaknapsackproblem01.ProcessInfo;
import cz.cvut.paaknapsackproblem01.Solver;
import java.math.BigInteger;

/**
 *
 * @author Vojtech Tosovsky
 */
public class BruteForceSolver extends Solver {

    /**
     * Row is one combination and the column indicate if the n-th stuff is
     * present in actual combination. Example of combination matrix when we have
     * three things. 3^2=8 111 110 101 100 011 010 001 000 The last index we
     * switch every 2^1 position The one before last we switch every 2^2
     * position .....
     *
     */
//    private boolean[][] matrix;
    private long[] switchers;
    private long[] powers;
    private Boolean actualValue[];
    private long numberOfCombinations;

    @Override

    public Knapsack solve(ProcessInfo processInfo) {
        createSwitchersAndPowersArray();
        long notTestedCombinations = numberOfCombinations;
        Knapsack bestKnapsack = new Knapsack(null);
        bestKnapsack.toatlPrice = -1;
        bestKnapsack.totalWeight = Integer.MAX_VALUE;

        while (!(notTestedCombinations == 0)) {
//            reInitSwitchers();
            notTestedCombinations--;
            boolean[] usedstuff = new boolean[stuffCnt];
            Knapsack candidate = nextKnapsack(usedstuff);
//            if ((candidate.toatlPrice > bestKnapsack.toatlPrice) && (!candidate.isOverload())) {
//                bestKnapsack = candidate;
//            }
            if ((candidate.toatlPrice >= bestKnapsack.toatlPrice) && (!candidate.isOverload())) {
                if ((candidate.toatlPrice > bestKnapsack.toatlPrice) || (candidate.totalWeight < bestKnapsack.totalWeight)) {
                    bestKnapsack = candidate;
                }
            }

        }

        return bestKnapsack;

    }

    private Knapsack nextKnapsack(boolean[] usedstuff) {
//        debug();
        Knapsack candidate = new Knapsack(usedstuff);
//        System.out.println("");
        for (int i = 0; i < stuffCnt; i++) {
            //j-th things is present
            if (actualValue[i]) {
                candidate.addIStuff(i);
//                System.out.print(1);
            } else {
//                System.out.print(0);
            }
            if (switchers[i] == 1) {
                actualValue[i] = !actualValue[i];
                switchers[i] = (powers[i]);
            } else {
                switchers[i]--;
            }
        }
        return candidate;
    }

    private void createSwitchersAndPowersArray() {
        BigInteger two = BigInteger.valueOf(2);
        Long maxLong = Long.MAX_VALUE;
        int copmare = two.compareTo(new BigInteger(maxLong.toString()));
        if (copmare > 0) {
            throw new IllegalArgumentException("The count of stuff is too big to many combinantion, cant save in long");
        }
        numberOfCombinations = two.pow(stuffCnt).longValue();
        switchers = new long[stuffCnt];
        powers = new long[stuffCnt];
        actualValue = new Boolean[stuffCnt];
        for (int i = 0; i < switchers.length; i++) {
            powers[i] = two.pow(i).longValue();
            actualValue[i] = true;
            switchers[i] = powers[i];
        }
    }

    private String printBigIntegers(BigInteger[] pole) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < pole.length - 1; i++) {
            builder.append(pole[i].intValue() + ",");
        }
        builder.append(pole[pole.length - 1]);
        return builder.toString();
    }
}

//    private void debug() {
//        debugP();
//        debugS();
//    }
//
//    private void debugS() {
//        for (int i = 0; i < switchers.length - 1; i++) {
//            System.out.print(switchers[i].intValue() + ",");
//        }
//        System.out.println(switchers[switchers.length - 1]);
//    }
//
//    private void debugP() {
//        for (int i = 0; i < powers.length - 1; i++) {
//            System.out.print(powers[i].intValue() + ",");
//        }
//        System.out.println(powers[powers.length - 1]);
//    }
