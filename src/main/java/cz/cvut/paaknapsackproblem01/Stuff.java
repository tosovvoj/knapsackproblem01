/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.paaknapsackproblem01;

/**
 *
 * @author Vojtech Tosovsky
 */
public class Stuff {

    public int price, weight, id;

    public Stuff(int price, int weight) {
        this.price = price;
        this.weight = weight;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Stuff{" + "price=" + price + ", weight=" + weight + '}';
    }

}
