/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.paaknapsackproblem01;

import Annealing.SAProcessInfo;
import Dynamic.DynamicSolver;
import java.awt.BorderLayout;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import javax.naming.spi.DirStateFactory;

/**
 *
 * @author Vojtech Tosovsky
 */
public class Evaluater {

    private Scanner scanner;
    private Solver solver;
    private int instanceID, stuffCnt, price;
    private Knapsack refSolution;
    private static boolean WARNING_IF_NOT_BEST = false;
    private int maxPrice = 0;
    private Solver refSolver = new DynamicSolver();

    public Evaluater(Solver solver) {
        this.solver = solver;
    }

    public void initialise(String fileName, Solver solver) throws FileNotFoundException {
        solver.initialise("inst" + fileName + ".inst.dat");
        refSolver.initialise("inst" + fileName + ".inst.dat");
//        scanner = new Scanner(new File("sol" + fileName + ".sol.dat"));

    }

    /**
     *
     * @return True if task was loaded, false if there is not any more task in
     * the file.
     */
    boolean loadTask() {
//        if (!scanner.hasNextInt()) {
//            return false;
//        }
        maxPrice = 0;
//        refSolution = getRefSolution();
        solver.loadTask();
        refSolver.loadTask();
        maxPrice = solver.maxPrice;

        return true;

    }

    private Knapsack getRefSolution() {
        instanceID = (scanner.nextInt());
        stuffCnt = (scanner.nextInt());
        price = (scanner.nextInt());
        boolean[] usedSutff = new boolean[stuffCnt];
        for (int i = 0; i < stuffCnt; i++) {
            int nextInt = scanner.nextInt();
            if (nextInt == 1) {
                usedSutff[i] = true;
            }
        }
        Knapsack knapsack = new Knapsack(usedSutff);
        knapsack.stuffCnt = stuffCnt;
        knapsack.toatlPrice = price;
        return knapsack;
    }

    public Result evaLuateOneInstance() throws InterruptedException {
        long timeInMicro = -1;
        System.gc();
        Thread.sleep(10);
        System.gc();
        Thread.sleep(10);
        System.gc();
        Thread.sleep(10);
        System.gc();
        Thread.sleep(30);
        long start = System.nanoTime();
        SAProcessInfo processInfo = new SAProcessInfo();
        Knapsack knapsack = solver.solve(processInfo);
        long end = System.nanoTime();
        Knapsack SDsolve = refSolver.solve(null);
//        System.out.println("");
//        System.out.println(end - start);
//        System.out.println("");
        double mistake = 0;
//        mistake = (refSolution.toatlPrice - knapsack.toatlPrice) / (double) refSolution.toatlPrice;
        mistake = (SDsolve.toatlPrice - knapsack.toatlPrice) / (double) SDsolve.toatlPrice;
//        mistake = warnIfNotBeast(knapsack, mistake);
//        System.out.print(mistake + ";");
        long timeInNano = end - start;
        timeInMicro = timeInNano / 1000;
        long timeInMili = timeInMicro / 1000;
        long second = timeInMili / 1000;
//        System.out.println("Time of comuputation was:=" + timeInNano + "nanoseconds");
//        System.out.println("Time of comuputation was:=" + timeInMicro + "microseconds");
//        System.out.println("Time of comuputation was:=" + timeInMili + "miliseconds");
//        System.out.println("Time of comuputation was:=" + second + "second");
//        System.out.println("God solution is " + knapsack.toString());
        Result result = new Result(timeInMicro, mistake, instanceID, processInfo);
//        System.out.println(instanceID);
        result.maxPrice = maxPrice;
//        System.out.println(mistake);
        return result;
    }

    private double warnIfNotBeast(Knapsack knapsack, double mistake) {
        if (WARNING_IF_NOT_BEST) {
            if (!(refSolution.usedStuffToString().equals(knapsack.usedStuffToString()))) {
                if (refSolution.toatlPrice != knapsack.toatlPrice) {
                    System.out.println("");
                    System.err.println("something is wrong");
                    System.out.println("Instace id is: " + instanceID);
                    mistake = (refSolution.toatlPrice - knapsack.toatlPrice) / (double) refSolution.toatlPrice;
                    System.out.println("BAD solution is " + knapsack.toString());
                    System.out.println("REf solution is " + refSolution.toString());
                    System.out.println("");
                } else {
//                System.out.println("Warning same price but another stuff");
                }
            }
        }
        return mistake;
    }
}
