/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.paaknapsackproblem01.Heurisitc;

import cz.cvut.paaknapsackproblem01.Stuff;

/**
 *
 * @author Vojtech Tosovsky
 */
public class StuffWithRatio extends Stuff {

    public Double ratio;
    public int id;

    public StuffWithRatio(int price, int weight, int id) {
        super(price, weight);
        ratio = price / (double) weight;
        this.id = id;
    }

}
