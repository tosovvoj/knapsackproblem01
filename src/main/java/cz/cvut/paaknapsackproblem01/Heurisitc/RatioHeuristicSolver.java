/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.paaknapsackproblem01.Heurisitc;

import cz.cvut.paaknapsackproblem01.Knapsack;
import cz.cvut.paaknapsackproblem01.ProcessInfo;
import cz.cvut.paaknapsackproblem01.Solver;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Vojtech Tosovsky
 */
public class RatioHeuristicSolver extends Solver {

    @Override
    public Knapsack solve(ProcessInfo processInfo) {
        List<StuffWithRatio> stuffArray = new ArrayList<StuffWithRatio>();
        for (int i = 0; i < weights.length; i++) {
            StuffWithRatio stuff = new StuffWithRatio(prices[i], weights[i], i);
            stuffArray.add(stuff);
        }
        Collections.sort(stuffArray, new Comparator<StuffWithRatio>() {
            public int compare(StuffWithRatio o1, StuffWithRatio o2) {
                return -o1.ratio.compareTo(o2.ratio);
            }
        });
        boolean[] usedstuff = new boolean[stuffCnt];
        int totalPrice = 0, totalWeight = 0, actStuffCnt = 0;
        for (StuffWithRatio stuff : stuffArray) {
            totalPrice += stuff.price;
            totalWeight += stuff.weight;
            actStuffCnt += 1;
            usedstuff[stuff.id] = true;
            if (totalWeight > Knapsack.capacity) {
                totalPrice -= stuff.price;
                totalWeight -= stuff.weight;
                actStuffCnt -= 1;
                usedstuff[stuff.id] = false;
            }
        }
        Knapsack knapsack = new Knapsack(usedstuff);
        knapsack.stuffCnt = actStuffCnt;
        knapsack.totalWeight = totalWeight;
        knapsack.toatlPrice = totalPrice;
        return knapsack;
    }

}
