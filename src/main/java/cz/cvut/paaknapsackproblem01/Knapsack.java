 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.paaknapsackproblem01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author Vojtech Tosovsky
 */
public class Knapsack implements Comparable<Knapsack> {

    public static int capacity;
    public int stuffCnt = 0;
    public int toatlPrice = 0;
    public int totalWeight = 0;

    public boolean[] usedstuff;// = new Boolean[stuffCnt];

    public Knapsack(boolean[] usedstuff) {
        this.usedstuff = usedstuff;
    }

    public Knapsack(Knapsack knapsack, boolean copy) {
        stuffCnt = knapsack.stuffCnt;
        toatlPrice = knapsack.toatlPrice;
        totalWeight = knapsack.totalWeight;
        usedstuff = Arrays.copyOf(knapsack.usedstuff, knapsack.usedstuff.length);
    }

    public void addIStuff(int i) {
        totalWeight += Solver.weights[i];
        stuffCnt++;
        toatlPrice += Solver.prices[i];
        usedstuff[i] = true;
    }

    public void removeStuff(int i) {
        if (!usedstuff[i]) {
            throw new IllegalArgumentException("you can not remove stuff hat is not in knapsack");
        }
        totalWeight -= Solver.weights[i];
        stuffCnt--;
        toatlPrice -= Solver.prices[i];
        usedstuff[i] = false;

    }

    public int compareTo(Knapsack o) {
        return toatlPrice - o.toatlPrice;

    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Knapsack{" + "stuffCnt=");
        builder.append(stuffCnt);
        builder.append(", toatlPrice=");
        builder.append(toatlPrice);
        builder.append(", totalWeight=");
        builder.append(totalWeight);
        builder.append(", ");
        builder.append(usedStuffToString());
        return builder.toString();
    }

    public String usedStuffToString() {
        StringBuilder builder = new StringBuilder();
        builder.append(",\t usedstuff=");
        for (int i = 0; i < usedstuff.length - 1; i++) {
            if (usedstuff[i] != false) {
                builder.append(1);
            } else {
                builder.append(0);
            }
            builder.append(" ");
        }
        if (usedstuff[usedstuff.length - 1] != false) {
            builder.append(1);
        } else {
            builder.append(0);
        }
        return builder.toString();
    }

    public boolean isOverload() {
        if (totalWeight > capacity) {
            return true;
        }
        return false;
    }

    public boolean hasStuff(int stuffIndex) {
        return usedstuff[stuffIndex];
    }

}
