package cz.cvut.paaknapsackproblem01;

import Annealing.SAProcessInfo;
import Annealing.SimulateAnnealingSolver;
import BranchAndBound.BranchAndBoundSolver;
import Dynamic.FPTAS;
import Dynamic.DynamicSolver;
import cz.cvut.paaknapsackproblem01.BruteForce.BruteForceSolver;
import cz.cvut.paaknapsackproblem01.Heurisitc.RatioHeuristicSolver;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App {

    private static final long MAXTIME = 4 * 60 * 1000 * 1000;//4 minutes

    public static void main(String[] args) throws FileNotFoundException, InterruptedException {

//        int fileNumber[] = {4, 10, 15, 20, 25, 27, 30, 32, 35, 37, 40};
//        int fileNumber[] = {20, 25, 30,35,40,45,50};
//        int fileNumber[] = { 30,35,40,45,50,55};
        int fileNumber[] = {30,30};
//        Indicate if the last instance take too much time, so we are not going to compute any more
        boolean stop = false;
        int computedInsatces = 40;
        for (int instaceSizeIndex = 0; instaceSizeIndex < fileNumber.length; instaceSizeIndex++) {
            List<Result> results = new ArrayList<Result>(computedInsatces);
            int number = fileNumber[instaceSizeIndex];
//            Solver solver = new RatioHeuristicSolver();
            Solver solver = new SimulateAnnealingSolver();
//            Solver solver = new BruteForceSolver();
//            Solver solver = new BranchAndBoundSolver();
//            Solver solver = new FPTAS();
//            Solver solver = new DynamicSolver();
            Evaluater evaluater = new Evaluater(solver);
            evaluater.initialise("/knap_" + number, solver);//30 za 180 sekund
            int maxInstaces = computedInsatces + 1;//Because the first is when the gb is running
            while (evaluater.loadTask()) {
                Result result = evaluater.evaLuateOneInstance();
                results.add(result);
                if (result.time > MAXTIME) {
                    stop = true;
                    break;
                }
                maxInstaces--;
                if (maxInstaces == 0) {
                    break;
                }
            }
            if (stop) {
                break;
            }
            printAvgTime(results);
            printAvgMistake(results);
            printSAProcessInfo(results);
        }
    }

    private static void printTeoreticalMistake(List<Integer> maxPrice, int insSize, List<Double> mistakes, List<Result> results) {
        int power = FPTAS.roundingCoefficient;
        for (int j = 0; j < maxPrice.size(); j++) {
            double maxMistake = 100 * (((double) (insSize * (power)) / (double) maxPrice.get(j)));
            Double mistake = mistakes.get(j);
            mistake *= 100;
            if (mistake > maxMistake) {
                System.out.println("neeeeeeeeeee");
            }
            System.out.print(String.format("%.4f", maxMistake) + ";");
        }
        System.out.println("");
    }

    private static void printAVvgTARMistakes(List<Integer> maxPrice, int insSize, List<Double> mistakes, List<Result> results) {
        int power = FPTAS.roundingCoefficient;
        double maxRealMistake = 0;
        double sumTer = 0;
        double sumReal = 0;
        for (int j = 0; j < maxPrice.size(); j++) {
            double terMistake = 100 * (((double) (insSize * (power)) / (double) maxPrice.get(j)));
            double realMistake = 100 * mistakes.get(j);
            sumTer += terMistake;
            sumReal += realMistake;
            if (maxRealMistake < realMistake) {
                maxRealMistake = realMistake;
            }
            if (terMistake < realMistake) {
                System.out.println("Neeeeeeee;");
            }
        }
//        System.out.println(String.format("%.4f", sumTer / mistakes.size()) + ";" + String.format("%.4f", sumReal / mistakes.size()) + ";" + String.format("%.4f", maxRealMistake));
        System.out.println((String.format("%.4f", sumTer / mistakes.size()) + "\t" + String.format("%.4f", sumReal / mistakes.size()) + "\t" + String.format("%.4f", maxRealMistake)).replaceAll(",", "."));
    }

    private static void printMistake(List<Double> mistakes) {
        for (int j = 0; j < mistakes.size(); j++) {
            Double mistake = mistakes.get(j);
            mistake *= 100;
            System.out.print(String.format("%.4f", mistake) + ";");
        }
        System.out.println("");
    }

    private static void printTime(List<Long> elapsedTime) {
        for (int j = 0; j < elapsedTime.size(); j++) {
            Long time = elapsedTime.get(j);
            System.out.print(time + ";");
        }
        System.out.println("");
    }

    private static void printAvgTime(List<Result> result) {
        int sum = 0;
        for (int j = 0; j < result.size(); j++) {
            sum += result.get(j).time;
        }
        System.out.println(sum / result.size());
    }

    private static void printSAProcessInfo(List<Result> results) {
        SAProcessInfo sAProcessInfo0 = ((SAProcessInfo) (results.get(0).processInfo).inofs.get("SA"));
        int steps = sAProcessInfo0.temperature.size();
        ArrayList<Double> temperature = new ArrayList<Double>(steps);
        ArrayList<Integer> aktPrice = new ArrayList<Integer>(steps);
        ArrayList<Integer> bestPrice = new ArrayList<Integer>(steps);
        for (int i = 0; i < steps; i++) {
            temperature.add(new Double(0));
            aktPrice.add(0);
            bestPrice.add(0);
        }
        for (int resultIndex = 0; resultIndex < results.size(); resultIndex++) {
            SAProcessInfo aktSAProcessInfo = ((SAProcessInfo) (results.get(resultIndex).processInfo).inofs.get("SA"));
            for (int j = 0; j < steps; j++) {
                temperature.set(j, (temperature.get(j) + aktSAProcessInfo.temperature.get(j)));
                aktPrice.set(j, (aktPrice.get(j) + aktSAProcessInfo.aktPrice.get(j)));
                bestPrice.set(j, (bestPrice.get(j) + aktSAProcessInfo.bestPrice.get(j)));
            }

        }
        for (int j = 0; j < results.size(); j++) {
            temperature.set(j, temperature.get(j) / results.size());
//            System.out.println("Step: \t: " + j + " temperature: \t" + (String.format("%.4f", temperature.get(j))) + " bestPrice: \t" + bestPrice.get(j)  + " aktPrice: \t" + aktPrice.get(j));
//            System.out.println( j + "\t" + (String.format("%.4f", temperature.get(j))) + "\t" + bestPrice.get(j)  + "\t" + aktPrice.get(j));
            System.out.println(((String.format("%.4f", temperature.get(j))) + "\t" + bestPrice.get(j) + "\t" + aktPrice.get(j)).replaceAll(",", ","));
        }

    }

    private static void printAvgMistake(List<Result> results) {
        double sum = 0;
        for (int j = 0; j < results.size(); j++) {
            sum += results.get(j).mistake;
        }
        System.out.println((String.format("%.4f", sum / results.size())).replaceAll(",", "."));
    }

}
