/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Annealing;

import cz.cvut.paaknapsackproblem01.Knapsack;
import cz.cvut.paaknapsackproblem01.ProcessInfo;
import cz.cvut.paaknapsackproblem01.Solver;
import java.util.Random;

/**
 *
 * @author Vojtech Tosovsky
 */
public class SimulateAnnealingSolver extends Solver {

    private static final Random random = new Random();
    private final int metropolisMax = 30;
    private final double startingTemerature = 25;
    private double temperature;
    private final double minTemperature = 0.03;
    private double collingRate = 0.9;
    private final int maxSwitchedThings = 5;
    private int actulaCntSwThings = 0;
    private final int[] actSwThings = new int[maxSwitchedThings];
    private Knapsack anKnapsack;
    private Knapsack bestKnapsack;
    private SAProcessInfo processInfo;

    @Override
    public Knapsack solve(ProcessInfo processInfo) {
        this.processInfo = new SAProcessInfo();
        anKnapsack = randomInitilaizeKnapsck();
        bestKnapsack = new Knapsack(anKnapsack, true);
        int before = anKnapsack.toatlPrice;
        anneal();
        int after = anKnapsack.toatlPrice;
        if (before != after) {
//            System.out.println("Before " + before + "After: " + after);
        }
//        return anKnapsack;
        processInfo.inofs.put("SA", this.processInfo);
        return bestKnapsack;
    }

    private Knapsack randomInitilaizeKnapsck() {
        boolean[] usedstuff = new boolean[stuffCnt];
        Knapsack knapsack = new Knapsack(usedstuff);
        for (int i = 0; i < stuffCnt; i++) {
            if (random.nextBoolean()) {
                knapsack.addIStuff(i);
                if (knapsack.isOverload()) {
                    knapsack.removeStuff(i);
                }
            }
        }
        return knapsack;
    }

    private void metropolis() {
        for (int i = 0; i < metropolisMax; i++) {
            double priceImprovment = randomChangeKnapsack();
            if (priceImprovment > 0) {
                if (bestKnapsack.toatlPrice < anKnapsack.toatlPrice) {
                    bestKnapsack = new Knapsack(anKnapsack, true);
                }
            }
            double criterion = Math.exp(priceImprovment / temperature);
            if (!(random.nextDouble() < criterion)) {
                undoLastChange();
            }
        }
    }

    public void anneal() {
//        System.out.println("Staritng with: " + this.parametersToString());
        temperature = startingTemerature;
        int cnt = 0;
//        System.out.println(((" cnt " + "\t" + "temperature" + "\t" + "akt.toatlPrice" + "\t" + "best.toatlPrice")).replaceAll(",", "."));
        while (temperature > minTemperature) {
            processInfo.temperature.add(temperature);
            processInfo.aktPrice.add(anKnapsack.toatlPrice);
            processInfo.bestPrice.add(bestKnapsack.toatlPrice);
//            System.out.println(("" + cnt + "\t" + temperature + "\t" + anKnapsack.toatlPrice + "\t" + bestKnapsack.toatlPrice).replaceAll(",", "."));
            cnt++;
            metropolis();
            temperature *= collingRate;
        }
//        System.out.println("Cnt: " + cnt + "Temperature: " + temperature + " collingRate: " + collingRate + " metropolisMax:" + metropolisMax + "minTemperature:" + minTemperature);
    }

    private double randomChangeKnapsack() {
        int priceBefore = anKnapsack.toatlPrice;
        actulaCntSwThings = random.nextInt(maxSwitchedThings);
        int i = 0;
        for (; i < actulaCntSwThings; i++) {
            actSwThings[i] = random.nextInt(stuffCnt);
        }
        if (i != maxSwitchedThings) {
            actSwThings[i] = -1;
        }
        switchStuff();
        if (anKnapsack.isOverload()) {
            switchStuff();
        }
        int priceAfter = anKnapsack.toatlPrice;
        return (priceAfter - priceBefore);
    }

    private void switchStuff() {
        for (int i = 0; i < actSwThings.length; i++) {
            int stuffIndex = actSwThings[i];
            if (stuffIndex != -1) {
                if (anKnapsack.hasStuff(stuffIndex)) {
                    anKnapsack.removeStuff(stuffIndex);
                } else {
                    anKnapsack.addIStuff(stuffIndex);
                }
            } else {
                return;
            }

        }

    }

    private void undoLastChange() {
        switchStuff();
    }

    public String parametersToString() {
        return "SimulateAnnealingSolver{" + "metropolisMax=" + metropolisMax + ", startingTemerature=" + startingTemerature + ", minTemperature=" + minTemperature + ", collingRate=" + collingRate + ", maxSwitchedThings=" + maxSwitchedThings + '}';
    }

}
